# On-Call Handovers

### Overview
This project contains issues for each SRE's on-call shift. Information is available on the handbook page, https://handbook.gitlab.com/handbook/engineering/infrastructure/team/ops/on-call-handover/

### Source-code
Issues in this project are created automatically with source code from https://gitlab.com/gitlab-com/gl-infra/woodhouse
